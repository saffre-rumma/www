.. |br| raw:: html

   <br />


.. |calendar| image:: /../docs/.static/freesvg/1538296242.svg
    :alt: Calendar
    :height: 40

.. |accounting| image:: /../docs/.static/freesvg/mono-ledger.svg
    :alt: Accounting
    :height: 40

.. |invoicing| image:: /../docs/.static/freesvg/primary-template-invoice.svg
    :alt: Invoicing
    :height: 40

.. |working| image:: /../docs/.static/freesvg/1538296242.svg
    :alt: Work time
    :height: 40

.. |reception| image:: /../docs/.static/freesvg/aiga_waiting_room.svg
    :alt: Reception
    :height: 40

.. |family| image:: /../docs/.static/freesvg/Family_of_Four.svg
    :alt: Family
    :height: 40

.. |social| image:: /../docs/.static/freesvg/Social-service-icon.svg
    :alt: Social service
    :height: 40

.. |presentation| image:: /../docs/.static/freesvg/primary-scheme-presentation.svg
    :alt: Presentation
    :height: 40

.. |handshake| image:: /../docs/.static/freesvg/1520519560.svg
    :alt: Handshake
    :height: 40

.. |busy| image:: /../docs/.static/freesvg/busy.svg
    :alt: Busy
    :height: 40
