========
Software
========

.. this section is no longer maintained

Our work with Lino makes us use many other software products, for which  we
provide expert service and support.

.. toctree::
    :maxdepth: 1

    linux
    python
    django
    sphinx
