============
Our services
============


.. _ss.services.lino:

Lino
====

We help you to set up and host your `Lino site <https://www.lino-framework.org>`__.

Easiest formula: we do the hosting for you. We care about backups, availability
and security of your Lino site.

As your Lino site grows and the number of users increases, you might reach a
point where you want to care yourself about hosting it. We encourage this
evolution and eventually invite you to grow into a self-hosting Lino site owner.
We give support to your :term:`server administrator` so that you become and
remain the sovereign owner of your database.



..
  Read more about Lino on https://www.lino-framework.org
  `Lino Welfare Product sheet (French) <https://www.saffre-rumma.net/lino/welfare>`__



.. No forced upgrades.

..
  Starting price is 42€ per month per user.
  "Per user" means the number of simultaneous user sessions: you may have an
  unlimited number of site users, but only a given number of them can access the
  site at a same moment.
  Price plus VAT per user per month with yearly invoicing.
  Adapted every year after the Belgian health price index (base January 2021).




.. _ss.services.tim:

TIM
===

We provide long-term maintenance and support for `TIM
<https://tim.lino-framework.org/>`__, the predecessor of Lino. We are proud to
say that TIM is being used since 1993 and still being used and loved in 2024.

TIM was written by Luc since 1993 for PAC Systems (renamed to
AbAKUS_ in 2008). It is a customizable software application for small and
medium-sized companies, used for invoicing, accounting, warehouse management,
etc. It is written in Clipper_ (now compiled using `Alaska xBase++`_).

.. _AbAKUS: https://www.abakusitsolutions.eu
.. _Clipper: https://en.wikipedia.org/wiki/Clipper_(programming_language)
.. _Alaska xBase++: https://www.alaska-software.com/products/overview.cxp


.. .. epigraph::

  | Does a good farmer neglect a crop he has planted?
  | Does a good teacher overlook even the most humble student?
  | Does a good father allow a single child to starve?
  | Does a good programmer refuse to maintain his code?
  | -- `The Tao of Programming <https://www.mit.edu/~xela/tao.html>`__


.. _ss.services.other:

Other software
==============

Our work with Lino makes us use other software products, for which we provide
expert service and support.

We use **Linux** as operating system on our servers and our desktop computers.
We have more than 20 years of experience with the **Python** programming
language. We have been using the `Django framework
<https://docs.djangoproject.com>`__ and the **Sphinx** documentation tools since
their beginnings and have acquired expert-level knowledge.

We also offer support and training for Free Software desktop applications like
LibreOffice, Thunderbird, Firefox, GIMP and Inkscape.
