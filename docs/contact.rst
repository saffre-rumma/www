=======
Contact
=======

The owner and responsible publisher of this website is Rumma & Ko OÜ.

Official address:

| Rumma & Ko OÜ
| Uus tn 1
| Vana-Vigala küla
| Märjamaa vald
| 78003 Raplamaa
| Estonia
|
| E-Mail *info at saffre-rumma.net*
| GSM: +372 / 5667 2435
| GSM: +32 / 475 36 85 13

.. Office Vigala: +372 / 4824 548
.. Office Tallinn: +372 / 6521 605

National registry number `10501849 <http://www.teatmik.ee/et/info/10501849>`_

VAT id: EE100588749

This website does not use any cookies nor does it collect any data
about you.

You may also contact directly a member of :doc:`our team </team/index>`.

.. Or contact any of the organizations and individuals who use Lino or contribute
   to it.

.. For non-technical and commercial information about Lino
   please contact :doc:`rumma`.

.. Subscribe to one of our mailing lists:
   `news <https://lino-framework.org/cgi-bin/mailman/listinfo/news>`__
   or
   `lino-developers <https://lino-framework.org/cgi-bin/mailman/listinfo/lino-developers>`__.

.. - We are on `lino-core:matrix.org <https://matrix.to/#/#lino-core:matrix.org?via=matrix.org>`__
