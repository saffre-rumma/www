=================
Lino
=================

Our favourite tool is the Lino framework, which we developed as a tool to be
shared with other software development providers like us.  See :doc:`/lsf`.

Lino features a wide range of highly customizable and integrated modules for

- contacts management
- calendar -- fully integrated into your other data
- reception and waiting room management
- enrolment management for activities, courses, events or travels
- standard office functionality -- uploads, excerpts, comments, summaries, reports
- client coaching
- households and family members
- job search assistance -- skills, career and curriculum
- invoicing -- automatically generate invoices
- accounting -- general ledger, purchases, bank statements, payment orders, VAT declarations
- issue tracker and project management
- work time tracking and service reports

Read more about Lino on https://www.lino-framework.org

.. toctree::
    :hidden:

    cosi
    voga
    tera
    welfare
    noi
    amici
