=======
Welcome
=======

.. sidebar::

  Contrairement à beaucoup d’autres entreprises du domaine informatique, Rumma &
  Ko ne recherche pas le profit à tout prix et c'est assez exceptionnel pour le
  souligner. (Mathieu 2020-12)

.. .. sidebar::
    Speak your truth quietly and clearly; and listen to others,
    even to the dull and ignorant; they too have their story.
    -- from `Desiderata <https://en.wikipedia.org/wiki/Desiderata>`_


Welcome to the website of **Rumma & Ko Ltd**.

..
  help our customers to use
  :doc:`Lino </services/lino4one>` and :doc:`TIM </services/tim>`.
  We provide professional hosting of :doc:`small Lino sites </services/lino4all>`.
  We are the motor of the :term:`Lino community`.
  And there is :doc:`more to say about us </about/index>`.

  .. rubric:: You want to become our customer if

  - you need a :term:`customized database application`
  - you don't expect a finished product and want to participate in :term:`analysis`
    and :term:`testing`
  - you want a stable and long-term solution with low maintenance costs
  - you want to remain the master of your data even when our collaboration
    would come to an end
  - you want the software created by our common efforts to be shared
    with others as :term:`Sustainably Free Software`.

  .. _partners:

  .. rubric:: You want to collaborate with us if

  - you develop software professionally and want to learn how to do your job using
    `sustainably free software <https://www.synodalsoft.net/free/>`__.

  - you want to help your customers with setting up and using
    `one of the existing Lino applications <https://www.lino-framework.org/apps.html>`__

.. rubric:: News

.. blogger_latest::

.. rubric:: Sitemap

.. toctree::
    :maxdepth: 1

    about/index
    services
    /team/index
    lsf
    /blog/index
    contact

.. toctree::
    :hidden:

    /partners
    /jobs/index
    /lino/index
    /sw/index
