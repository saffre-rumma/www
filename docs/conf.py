# -*- coding: utf-8 -*-
from pathlib import Path
from atelier.sphinxconf import configure ; configure(globals())
from lino.sphinxcontrib import configure ; configure(globals())

# General substitutions.
project = 'Saffre-Rumma'
import datetime
copyright = '2011-{} Rumma & Ko Ltd'.format(datetime.date.today().year)
html_title = "Rumma & Ko"
htmlhelp_basename = 'saffre-rumma'

# same logo is being used for all languages
# html_logo = "logo.jpg"
html_logo = str(Path('../docs/.static/rumma100.png').resolve()) # pandas

html_context.update({
    'display_gitlab': True,
    'gitlab_user': 'saffre-rumma',
    'gitlab_repo': 'www',
})

# html_context.update(public_url='https://www.saffre-rumma.net')

if html_theme == "insipid":
    html_theme_options = {
        # 'body_max_width': None,
        # 'breadcrumbs': True,
        'globaltoc_includehidden': False,
        'left_buttons': [
            'search-button.html',
            'home-button.html',
            'languages-button.html',
        ],
        'right_buttons': [
            'fullscreen-button.html',
            # 'repo-button.html',
            # 'facebook-button.html',
        ],
    }
    html_sidebars = {
        '**' : [
            #'languages.html',
            'globaltoc.html',
            'separator.html',
            'searchbox.html',
            'indices.html',
            'links.html'
            ]}
