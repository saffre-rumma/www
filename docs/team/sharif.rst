.. _sharif:

=============
Sharif Mehedi
=============

.. image:: /images/sharif.jpeg
         :alt: Sharif
         :width: 30%
         :align: right

Status: Freelance contributor.
Paid :term:`core developer` and :term:`server administrator`
from February 2021 to today.

Achievements: Turned Lino into asynchronous.
Introduced JavaScript internationalization.
Contributed many optimizations of the :term:`React front end`.
Designed the SPARQL statements for getting place and country names from Wikidata.

Expert knowledge in Linux, Git, Python, Django, Docker, JavaScript, CSS, ExtJS,
ReactJS. Broad knowledge about many technologies.

Work fields: Coding, testing, deployment, hosting, end-user support.

Soft skills: Good team player. Able to make compromises.
Quick learner. Flexible.
A polite and attentive teamworker.
Independent thinker. Creative.


Location: `Cumilla (Bangladesh) <https://en.wikipedia.org/wiki/Comilla>`__

Contact:
sharif@saffre-rumma.net |
`GitHub <https://github.com/8lurry>`__ |
`GitLab <https://gitlab.com/8lurry>`__ |
`LinkedIn <https://www.linkedin.com/in/8lurry/>`__


See also:
- :doc:`/blog/2021/0413`
