.. _luc:

==========
Luc Saffre
==========

.. image:: /images/luc.jpg
   :alt: Luc
   :width: 30%
   :align: right

Status: CEO. Senior developer. Project manager. Author of Lino_ and TIM_.

Expert knowledge in Linux, Git, Python, Django, CSS and ExtJS.

Work fields: Coding, testing, deployment, hosting, end-user support, accounting,
sales, team management.

Soft skills:
Wide-spread knowledge and experience in software development.
Persistent.
Open to critics.
Resistant to despair.

See also: https://luc.lino-framework.org/about

Location: Tallinn/Vigala (Estonia)

Contact: luc@saffre-rumma.net or via `GitLab <https://gitlab.com/orgs/lino-framework/people/lsaffre>`__


.. _TIM: https://tim.lino-framework.org/129.html
.. _Lino: https://www.lino-framework.org
.. _Django: https://www.djangoproject.org
.. _ExtJS: https://www.sencha.com/products/extjs/
