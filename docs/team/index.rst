========
Our team
========


.. |br| raw:: html

   <br />


Active workers
==============

.. list-table::
  :widths: 20 80

  * - .. image:: /images/luc.jpg
         :alt: Luc
         :width: 80pt

    - :doc:`luc`
      |br| CEO, senior developer, project manager.
      |br| luc@saffre-rumma.net or via `GitLab <https://gitlab.com/lsaffre>`__


  * - .. image:: /images/hannes.png
         :alt: Hannes
         :width: 80pt

    - **Hannes Liivat**
      |br| System administrator, customer support.
      |br| hannes@saffre-rumma.net


  * - .. image:: /images/sharif.jpeg
         :alt: Sharif
         :width: 80pt

    - :doc:`sharif` (en, bn)
      |br| Freelance contributor in **Kadaba (Bangladesh)**.
      |br| sharif@saffre-rumma.net
      or via `GitLab <https://gitlab.com/8lurry>`__

  * - .. image:: /images/ly.jpg
         :alt: Ly
         :width: 80pt

    - **Ly Rumma**
      |br| Web designer, company founder and woman behind the
      scenes who holds it all together.




Trusted contributors
====================

.. list-table::
  :widths: 20 80

  * - .. image:: /images/hamza.png
         :alt: Hamza
         :width: 80pt

    - :doc:`hamza` (en, fr, ar)
      |br| Core developer and consultant in **Monastir (Tunisia)**.
      |br| hamza@saffre-rumma.net |
      `GitHub <https://githab.com/khchine5>`__ |
      `GitLab <https://gitlab.com/khchine5>`__ |
      `LinkedIn <https://www.linkedin.com/in/hamza-khchine-10ab6268/>`__


  * - .. image:: /images/tonis.jpg
         :alt: Tonis
         :width: 80pt

    - :doc:`tonis` (en, et)
      |br| Core developer and consultant in **Tallinn (Estonia)**.
      Paid worker from June 2018 to June 2020.
      |br| tonis@saffre-rumma.net |
      `GitHub <https://githab.com/tonis.piip>`__ |
      `GitLab <https://gitlab.com/tonis.piip>`__ |
      `LinkedIn <https://www.linkedin.com/in/tonis-piip-8b32461b1/>`__


.. toctree::
    :hidden:

    luc
    hamza
    tonis
    sharif
