.. _hamza:

=============
Hamza Khchine
=============

.. image:: /images/hamza.png
         :alt: Hamza
         :width: 30%
         :align: right

Status: Trusted contributor.
Paid :term:`core developer` and :term:`server administrator`
from September 2015 to June 2020.

Summary: A pragmatic engineer who finds working solutions
without turning the whole system upside down.

Expert knowledge in Linux, Git, Python, Django, JavaScript, CSS, ExtJS, ReactJS.

Work fields: Coding, testing, deployment, hosting, end-user support.

Soft skills: Good team player. Broad knowledge about many technologies.
Quick learner. Flexible.

Location: Monastir (Tunisia)

Contact:
hamza@saffre-rumma.net |
`GitHub <https://github.com/khchine5>`__ |
`GitLab <https://gitlab.com/khchine5>`__ |
`LinkedIn <https://www.linkedin.com/in/hamza-khchine-10ab6268/>`__
