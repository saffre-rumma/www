.. _tonis:

=============
Tonis Piip
=============

.. image:: /images/tonis.jpg
         :alt: Tonis
         :width: 30%
         :align: right

Status: Trusted contributor.
Paid :term:`core developer` and :term:`server administrator`
from June 2018 to June 2020.

Summary: The father of the :term:`React front end`.

Expert knowledge in Linux, Git, Python, Django, CSS, ExtJS, ReactJS.

Work fields: Coding, testing, deployment, hosting, end-user support.

Soft skills: Good team player. Broad knowledge about many technologies.
Quick learner. Flexible.

Location: Tartu (Estonia)

Contact:
tonis@saffre-rumma.net |
`GitHub <https://githab.com/tonis.piip>`__ |
`GitLab <https://gitlab.com/tonis.piip>`__ |
`LinkedIn <https://www.linkedin.com/in/tonis-piip-8b32461b1/>`__
