========================
The Synodalsoft project
========================

We want our intellectual work to be sustainably free: usable for everybody and
forever. That's why we publish all our software under the *GNU Affero General
Public License* (AGPL), the most :term:`protective free software license` there
is.

Lino needs more than a few developers. We want the Lino community to become big,
but we have no plans of becoming *ourselves* big. See also :doc:`/about/views`.

.. - Find an existing organization

.. From our
  beginnings we wanted Lino to be shared with other software development providers
  like us.

If the Lino community gets bigger than we can manage, we plan to create a
non-profit organization to which we transfer our copyright so that it would
become the legal owner of Lino.

.. We did not yet find any candidate recipient.

..
  Owning the copyright of a software product requires work and means
  responsibility.

..
  The FSF satisfies our condition and has a process in place for such donations
  (`GNU Software Evaluation page <https://www.gnu.org/help/evaluation.html>`__),
  but already the application process would require much work from our side and we
  are afraid that they would refuse our donation because Lino doesn't "fit to
  their portfolio". The world of Free Software is diverse, and not everybody can
  directly collaborate with everybody else.

We work already now on expressing how such a non-profit organization would look
like. The `synodalsoft.net <https://www.synodalsoft.net>`__ website is our
suggestion.

https://www.synodalsoft.net




..
  Meanwhile
  We didn't yet do this because a non-profit organization causes more
  administrative work than our existing family-sized private company.

..
  Synodalsoft does not yet exist legally because running a legal person would
  cause significant administrative costs. That's why our work is currently
  coordinated by :ref:`rumma`, a family-sized company in Estonia. Here is their
  statement: https://www.saffre-rumma.net/lsf/

..
  Synodalsoft is the legal entity that might be act as the :term:`product carrier` of
  the Lino framework and as the motor of the :term:`Lino community`.
