===================
20 years of success
===================

Our activity started in **2001** when Luc, after moving from Belgium to Estonia,
realized that neither he nor the users of TIM want to stop using it.

The **first years** were marked by the switch from BEF to EUR for most of our
customers. The euro was introduced in Belgium on January 1st 2002. During this
period we also started using Python with the "TIM Tools", a series of small
programs used by TIM to generate PDF files or to print on a Windows printer.

Since **2009** we invest most of our energy into Lino.

In **January 2011** the first real-world Lino application went into production
in a Belgian *Public Centre for Social Welfare* (PCSW).

In **September 2012** he registered the domain name `lino-framework.org`.

In **March 2014** we started our second Lino production site.

In **September 2015**, Hamza discovered Lino and joined our team.

In **January 2016** we started  the :ref:`voga`
project with  Alexa who worked as a
volunteer analyst almost full-time during eight months.

In **December 2016** we started the :ref:`avanti` project.

In **February 2017** `Tonis <https://github.com/TonisPiip>`__ started working
for us as the second full-time developer.

In **June 2018** Hamza started working full-time for us.


.. .. image:: /images/2019/foto-2.jpg
  :width: 40%
  :align: right


In **June 2020**, we sent Tonis and Hamza out into the world
in order to share their know-how to other companies.
They continue to help our team occasionally when needed.
Read the details in Luc's blog post
`Lino reaches a new level
<https://luc.lino-framework.org/blog/2020/0628.html>`__.

In **October 2020**, Hannes Liivat started working full-time as our system
administrator.

In **February 2021**, :doc:`Sharif Mehedi </team/sharif>` started working
full-time as  paid :term:`core developer`.
