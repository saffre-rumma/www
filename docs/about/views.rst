===========
Our choices
===========

Some of our choices

- We are a small organization and want to remain human size. We will rather
  multiply than become big.

- We concentrate on a limited number of products for which we can
  ensure our competence.

- We refuse to consider our spiritual work as private property.

- Besides engaging people ourselves, we also provide free **mentoring** and
  encourage independent actors to learn how we make money and to use this
  knowledge to make their own money.

- Our business model is based on these choices.

Read more on our Synodalsoft website :ref:`ss.how`.
