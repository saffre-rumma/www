==========
Rumma & Ko
==========

**Rumma & Ko OÜ**  is a small limited private share company (osaühing) in
Estonia having most of their customers in Belgium.
It is owned by Belgian software developer Luc Saffre and his wife Ly Rumma.

**We make money** by helping our customers to develop and use their :term:`customized database applications
<customized database application>`. We do this using the :ref:`Lino framework <ss.services.lino>`.
We also sell support for :ref:`other free software <ss.services.other>`.

**We want Lino to become big.**
We help other software developers to make money using our software and business model.
We are the motor of the :doc:`Synodalsoft project </lsf>`.


..
  We help our customers to use
  :doc:`Lino </services/lino4one>` and :doc:`TIM </services/tim>`.
  We provide professional hosting of :doc:`small Lino sites </services/lino4all>`.

..
  We are the motor of the `Lino community
  <https://community.lino-framework.org/>`__ and

**We are small and plan to remain so.**
Besides Luc and Ly we engage one full-time :term:`system administrator`
and maintain a small world-wide network of independent Lino developers.
See :doc:`/team/index`.

..
  Our team is our capital. Our workers are talented, dedicated and diverse. Most
  of them live in countries where living costs less. We help them develop the
  economy of their home country.

..
  When you want our team to help you individually, then
  you want to be our customer and to pay a just price for what you get. That price
  is synodal: we and you discover it together step by step.


**We have been doing this since 2001** and can look back at :doc:`/about/history`.


.. toctree::
    :maxdepth: 1

    views
    history
