==========
Willkommen
==========


**Rumma & Ko OÜ** ist eine GmbH mit Firmensitz in Estland.
Wir helfen unseren Kunden bei der Benutzung von
:doc:`Lino </services/lino4one>` und
:doc:`TIM </services/tim>`.
Wir bieten professionelle  :doc:`Beherbergung von kleinen Lino-Anlagen
</services/lino4all>`. Und :doc:`es gibt noch mehr zu sagen </about/index>` über
uns.

.. rubric:: Newsticker

.. blogger_latest::

.. rubric:: Sitemap


.. toctree::
    :maxdepth: 1

    services/index
    about/index
    lino/index
    sw/index
    jobs/index
    contact
