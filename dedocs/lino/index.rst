====
Lino
====

Lino ist ein Framework zum Entwickeln von maßgeschneiderten
Datenbankanwendungen.

Lino als Framework bietet die ideale Infrastruktur, mit der
Grundfunktionalitäten wie Benutzerarten, Zugriffsrechte, An­sichtsparameter,
Ar­beitsabläufe, Ausdrucke, mehrsprachige Benutzerschnittstelle und
mehrsprachige Kontakte usw. konfigurierbar sind.

Die Anwendung wird als Freie Software veröffentlicht und kann potentiell
auch durch andere Organisationen und/oder Entwickler weiter genutzt bzw.
verändert werden. Der Auftraggeber ist nicht an den Dienstleister gebunden
und kann sowohl Entwicklung als auch Wartung jederzeit einem anderen
Anbieter übertragen.


.. toctree::
    :maxdepth: 1

    voga
    cosi
    presto
    welfare
