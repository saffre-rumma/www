===========
Lino Presto
===========

Funktionsbeschreibung
=====================

Für jeden Klienten und jeden Arbeiter wird ein Datensatz angelegt, in dem alle
rele­vanten Daten zur Person erfasst werden.

Es gibt Verträge für Klienten, bei denen ein oder mehrere Arbeiter mehr oder
weniger regelmäßig bestimmte Arbeiten leisten gehen. Pro Vertrag kann Lino
automatisch Ter­minvorschläge erstellen, die dann im Kalender bestätigt oder
verändert werden kön­nen. Ein Termin wird zum Arbeitsbericht, wenn er als
ausgeführt markiert wurde.

Es gibt einen zentralen Kalender, dessen Einträge abrufbar sind von
verschiede­nen Stellen, u.A. pro Klient, pro Vertrag oder pro Arbeiter.

Lieferscheine für die verschiedenen Tätigkeiten, bei denen Dienstleistungen oder
Gegenstände direkt verkauft werden. Falls nötig werden ­spezi­fi­sche
Benutzerschnittstellen zur Erfassung entwickelt.

Lino erstellt automatisch Rechnungen auf Basis der Lieferscheine und
Arbeitsberich­te.

Optionen:

- Die Rechnungen können entweder in eine Buchhaltungssoftware exportiert werden,
  oder die Buchhaltung kann in Lino verwaltet werden.

Es gibt die Benutzerarten Sekretär, Arbeiter und Anlageverwalter.


`Project homepage <https://presto.lino-framework.org/>`__
