=========
Lino Voga
=========

Funktionsbeschreibung
=====================

Lino Voga ist Soft­wareanwendung zum Verwalten von Aktivitäten (z.B. Kurse oder
Reisen), inklusive Rechnungsausgang an die Teilnehmer und Erfassen der
Zahlun­gen, Einkaufsrechnungen und sonstigen Buchungen nach dem System der
dop­pel­ten Buch­führung.

Alle Teilnehmer, Kursleiter und Lieferanten werden als Partner erfasst. Jeder
Partner hat eine Postadresse sowie eine Liste von Kontaktdaten (Telefonnummern,
Email-Adressen usw.)

Ein Kurs hat eine Liste von Einschreibungen und eine Liste von Ter­minen. Die
Termine eines Kurses werden automatisch generiert (z.B. "Jeden zweiten Montag
von 19 bis 21 Uhr"), können jedoch auch manuell erfasst oder angepasst werden.
Pro Termin kann man die Anwesenheiten der Teilnehmer erfassen. Die
Einschreibungen zu einem Kurs haben einen Status (Angefragt, Bestätigt). Wenn
z.B. ein Kurs überfüllt ist, kann ein Teilnehmer sich trotzdem in die Warteliste
eintragen.

Der Kalender ist auf Wunsch online ein­seh­bar zwecks Terminplanung, Änderungen,
Raumbele­gung.

Bei der Einschreibung wird ein Tarif angegeben, aufgrund dessen Lino
Fakturie­rungs­vorschläge macht. Es gibt verschiedene Tarifsysteme: feste
Einschreibegebühr, Jah­reskarten, Zehnerkarten, Abonnements pro Termin oder pro
Anwesenheit.

Lino erstellt auf Wunsch Einschreibebestätigungen mit Rechnung und Liste der
ge­planten Termine.

Lino verwaltet auf Wunsch die Räume, in denen die Kurse stattfinden, und hilft
beim Vermei­den von Terminkollisionen. Die Räume kann man auch an externe
Veranstalter vermieten, was ebenfalls automatisch fakturiert werden kann.

Um das Kursangebot übersichtlich zu halten, definiert man Kursreihen, Themen und
Kursarten.

Lino erstellt auf Wunsch einen Tätigkeitsbericht über die stattgefundenen
Kurse, mit Anzahl Veranstaltungen und An­zahl Teilnehmern.

Außerdem auch alle Buchhaltungsfunktionen wie in :doc:`cosi`.

`Project homepage <https://voga.lino-framework.org/>`__
