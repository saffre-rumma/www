======================
Self-hosted Lino sites
======================

As your Lino site grows and the number of users increases, you might reach a
point where it is more efficient to care yourself about hosting it.

Rumma & Ko encourages this evolution.
