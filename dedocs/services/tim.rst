===========================
TIM maintenance and support
===========================

We provide long-term support for `TIM <https://tim.saffre-rumma.net>`__ the
venerable predecessor of :doc:`/lino/index`. Actively being used since 1992 and
still competitive and loved in 2021.
