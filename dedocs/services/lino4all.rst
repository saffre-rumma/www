.. include:: /../docs/include/freesvg.rst

=============
Lino für alle
=============

Lino bewährt sich seit 2010 in Betrieben ab 10 Benutzern (:doc:`lino4one`). Aber
auch kleinere Organisationen brauchen maßgeschneiderte Anwendungen. Weil diese
Betriebe über weniger Ressourcen verfügen, müssen sie sich informationstechnisch
oft mit suboptimalen Lösungen begnügen.

Wir bieten Komplettlösungen zu nachhaltigen Preisen für Lino-Anlagen ab 2
Benutzern.


.. grid:: 1 2 3 3

 .. grid-item::

  |invoicing| |accounting|

  .. rubric:: Lino Così -- So mögen wir Buchhaltung

  .. button-link:: /de/lino/cosi
    :expand:

    Angebote | Produkte | Fakturierung | Buchhaltung | MwSt-Erklärung

  40 €/Benutzer/Monat\ :superscript:`*`

 .. grid-item::

  |calendar| |invoicing| |accounting|

  .. rubric:: Lino Voga -- Lino für Trainingszentren

  .. button-link:: /de/lino/voga
    :expand:

    Aktivitäten | Kalender | Einschreibungen | Fakturierung | Buchhaltung

  45 €/Benutzer/Monat\ :superscript:`*`

 .. grid-item::

  |calendar| |invoicing| |accounting|

  .. rubric:: Lino Tera -- Lino für Therapiezentren

  .. button-link:: /de/lino/tera
    :expand:

    Klienten | Kalender | Fakturierung | Buchhaltung

  45 €/Benutzer/Monat\ :superscript:`*`

 .. grid-item::

  |calendar| |reception| |social| |handshake|

  .. rubric:: Lino Welfare -- |br| Lino für Sozialhilfezentren

  .. button-link:: /de/lino/welfare
    :expand:

    Klienten | Kalender | Empfang | Verträge | Hilfen | Kurse

  55 €/Benutzer/Monat\ :superscript:`*`

 .. grid-item::

  |working| |calendar|

  .. rubric:: Lino Noi -- Lino für Teams

  .. button-link:: /de/lino/noi
    :expand:

    Projektverwaltung | Ticket- und Arbeitseiterfassung | Kalender

  40 €/Benutzer/Monat\ :superscript:`*`

 .. grid-item::

  |family| |calendar|

  .. rubric:: Lino Amici  -- Lino für Familien

  .. button-link:: /de/lino/amici
    :expand:

    Contacts | Calendar

  35 €/Benutzer/Monat\ :superscript:`*`


:superscript:`*` Preise pro Benutzer pro Monat bei jährlicher Fakturierung,
zzgl. MwSt. und indexiert nach belgischem Gesundheitsindex Basis Januar 2021.
"Benutzer" bedeutet hier die Anzahl der gleichzeitigen Sitzungen: Sie können
unbeschränkt viele Benutzer anlegen, aber nur eine vereinbarte Anzahl kann
gleichzeitig auf die Anlage zugreifen.

Für Neugierige bieten wir verführerische Bedingungen:

- Kostenlose Probephase: Wir richten Ihnen für maximal 3 Monate Ihre
  individuelle Probedaten­­bank ein, auf die nur Sie Zugriff haben.

- Keine Kaufverpflichtung: Wer nach Abschluss der Probe­phase nicht weitermachen
  will, sagt das einfach und wir löschen die Datenbank wieder.

- Support kostenlos im ersten Monat nach Vertragsabschluss.

Eine stabile Lino-Anlage läuft zu äußerst günstigen Gesamtbetriebskosten,
weil Sie quasi keinen Support brauchen, wenn Sie einmal eingearbeitet sind.
Es gibt keine erzwungenden Upgrades.
Jede Lino-Anlage ist maßgeschneidert und ändert sich nur auf Anfrage.
Andererseits ist und bleibt Lino ein mächtiges und
flexibles Werkzeug, das bei Bedarf mit ihren Ansprüchen wächst.




..  Mehrere unabhängige IT-Profis in Ostbelgien erzählen ihnen gerne mehr über Lino:

  - Arnold François (Tel +32 470 671715, arnold.francois@posteo.de)

  - Manuel Weidmann (`Code Gears <http://www.code-gears.com/>`__, GSM +32 474 730415, info@code-gears.com)

  - Steve Woellenweber (`Switcom <http://www.switcom.be/>`__, GSM +32 487 257692, info@switcom.be)

  - Gerd Xhonneux (`XTC <https://xtc.xhonneux.com/>`__, Tel:+49 24089819791, xtc@xhonneux.com)

  Oder melden Sie sich unverbindlich direkt beim Hersteller:
