from atelier.invlib import setup_from_tasks
ns = setup_from_tasks(
    globals(),
    use_dirhtml=True,
    tolerate_sphinx_warnings=False,
    # build_docs_command='./build_docs.sh',
    # doc_trees = ['docs', 'dedocs', 'frdocs'],
    # blogref_url="http://hw.lino-framework.org",
    revision_control_system='git',
    selectable_languages=('en','de','fr','et'),
    intersphinx_urls=dict(docs='https://www.saffre-rumma.net'))
