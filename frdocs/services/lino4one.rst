===========================
Fonctionnement auto-hébergé
===========================

L'opérateur d'un site de plus de 10 utilisateurs s'intéressera à l'option de
pouvoir gérer lui-même l'hébergement de leur site Lino.

We help our bigger customers to develop and maintain their own :term:`Lino
site`.

We give support to your :term:`server administrator` so that you become and
remain the sovereign owner of your database.

Lino is 100% sustainably free and open-source software.
