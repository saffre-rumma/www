.. include:: /../docs/include/freesvg.rst

==========================
Hébergement Lino pour tous
==========================

Nous offrons des solutions hébergement Lino à prix durabale pour des sites Lino
à partir de 2 utilisateurs à des conditions séduisantes:

- Phase d'évaluation gratuite: nous mettons en route votre site Lino individuel
  à votre disposition confidentielle.

- Aucune obligation d'achat: vous pouvez terminer la phase d'évaluation à tout
  moment et nous supprimerons vos données.

- Support gratuit pendant le premier mois après le début de contrat.

Lino reste un outil puissant et flexible qui croit avec vos besoins. Chaque site
Lino est taillé à la mesure de l'opérateur.  Pas de mises à jour obligées.


.. grid:: 1 2 3 3

 .. grid-item::

  |invoicing| |accounting|

  .. rubric:: Lino Così -- 40 €/utilisateur/mois\ :superscript:`*`

  Voici comment nous aimons la comptabilité

  .. button-link:: /fr/lino/cosi
    :expand:

    Offres | Produits | Facturation | Comptabilité | Déclaration TVA

 .. grid-item::

  |calendar| |invoicing| |accounting|

  .. rubric:: Lino Voga -- 45 €/utilisateur/mois\ :superscript:`*`

  .. button-link:: /fr/lino/voga
    :expand:

    Activités | Calendrier | Inscriptions | Facturation | Comptabilité

 .. grid-item::

  |calendar| |invoicing| |accounting|

  .. rubric:: Lino Tera - 45 €/utilisateur/mois\ :superscript:`*`

  pour centres thérapeutiques

  .. button-link:: /fr/lino/tera
    :expand:

    Clients | Calendrier | Facturation | Comptabilité

 .. grid-item::

  |calendar| |reception| |social| |handshake|

  .. rubric:: Lino Welfare -- 55 €/utilisateur/mois\ :superscript:`*`

  pour centres publics d'action sociale

  .. button-link:: /fr/lino/welfare
    :expand:

    Clients | Calendrier | Accueil | Contrats | Aides | Ateliers

 .. grid-item::

  |working|

  .. rubric:: Lino Noi -- 40 €/utilisateur/mois\ :superscript:`*`

  for teams where time is more than money

  .. button-link:: /fr/lino/noi
    :expand:

    Gestion de projects | Combined issue / work time tracking

 .. grid-item::

  |family| |calendar|

  .. rubric:: :doc:`Lino Amici </lino/amici>` - 35 €/utilisateur/mois\ :superscript:`*`

  pour toute la famille

  .. button-link:: /fr/lino/amici
    :expand:

    Contacts | Calendrier


:superscript:`*` Prix HTVA par utilisateur par mois en cas de facturation
annuelle, adapté chaque année selon l'Index Santé Belge (base janvier 2021).
"utilisateur" signifie le nombre maximal de sessions simultanées. On peut avoir
un nombre illimité d'utilisateurs, mais seulement le nombre convenu peuvent
accéder au site en même temps.

Par exemple pour un petit site avec 2 utilisateurs, un Lino Voga reviendrait à
45*2*12 € = 1080 € par année\ :superscript:`*`. C'est le prix forfaitaire pour
le hosting. S'y ajoute le support qui est facturé par minute avec un rapport
mensuel détaillé.
