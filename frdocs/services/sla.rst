=================================================
Contrats de support, maintenance et développement
=================================================

The collaboration between us and our customers is regulated by service level
agreements.

**Our services** include

- analysis -- we help you to formulate what you need
- development -- we write the application you need
- hosting -- we offer different Linux hosting services depending on your
  needs
- maintenance -- we help you to optimize your work and apply upgrades
  when you want them
- hotline -- we answer your questions about Lino
- training -- we write end-user documentation and organize trainings

**You want our services** if

- you need a database application and can't find any satisfying stock
  solution
- you don't expect a finished product and want to help with analysis
  and testing
- you want a stable and long-term solution
- you want to remain the master of your data even if our collaboration
  comes to an end
- you want the software created by our common efforts to be shared
  with others as open source software under a free license.
