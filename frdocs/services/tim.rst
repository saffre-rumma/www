===========================
Maintenance et support TIM
===========================

We provide long-term support for :doc:`/sw/tim`.

.. epigraph::

  | Does a good farmer neglect a crop he has planted?
  | Does a good teacher overlook even the most humble student?
  | Does a good father allow a single child to starve?
  | Does a good programmer refuse to maintain his code?
  | -- `The Tao of Programming <https://www.mit.edu/~xela/tao.html>`__
