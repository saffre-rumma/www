===
TIM
===

TIM is the predecessor of :doc:`/lino/index`. Actively being used since 1994 and
still competitive and loved in 2021.
