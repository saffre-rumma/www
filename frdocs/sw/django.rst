======
Django
======

Our favourite tool, :doc:`/lino/index`, is based on the `Django framework
<https://docs.djangoproject.com>`__. We have more than 10 years of experience
using it.
