====
Lino
====

Lino est un outil "framework" utilisé pour développer des logiciels
d'application sur mesure.



.. toctree::
    :maxdepth: 1

    welfare
    cosi
    voga
    tera
    noi
    amici
