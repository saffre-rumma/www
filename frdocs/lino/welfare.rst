============
Lino Welfare
============

Nous offrons une gamme de services d'hébergement, de support, de maintenance et
de développement  pour `Lino Welfare <https://fr.welfare.lino-framework.org>`__,
un logiciel libre pour centres publics d'action sociale.

Lino Welfare est une application informatique faite pour assistants sociaux d'un CPAS belge.
En production depuis janvier 2010
Fonctionne actuellement dans deux centres, celui d’Eupen et celui de Châtelet. D‘autres sont en préparation.
Multilingue et multiculturelle dès sa naissance
Extrèmement configurable et adaptable
Conçue pour servir partout en Belgique
Application libre

Charactéristiques
=================

.. toctree::
    :maxdepth: 1

    framework_specs
    welfare_specs


Images d'écran
==============

- `Bénéficiaires <https://welfare.lino-framework.org/fr/clients.html>`_

Sites démo
==========

- https://weleup1.mylino.net/
- https://welcht1.mylino.net/

Références
==========

- CPAS d'Eupen (087/63 89 50)
- CPAS de Châtelet (071/24.41.10 ou 071/24.41.41)

Pour en savoir plus
===================

- Contactez-nous: https://www.saffre-rumma.net/fr/contact

- Documentation technique (en anglais): https://welfare.lino-framework.org
