===================================
Charactéristiques du framework Lino
===================================

- Chaque site Lino Welfare est techniquement une application indépendante.
  Chaque CPAS décide individuellement ce qui lui faut et à quelle vitesse les
  choses changent.

- Une application Lino est un «site web» et fonctionne indépendamment du système
  d'exploitation et de l'endroit géographique.

- Lino utilise les technologies Python, Django et React pour délivrer des
  applications « Web » au look « desktop ».

- Lino tourne sur un serveur Linux et une bases de données MySQL ou PostgreSQL.

- Plusieures instances par serveur possible (par exemple pour différentes
  applications/services et/ou environnement de test)

- Multilingue à trois niveaux : (1) interface utilisateur (2) désignations des
  codes et (3) destinataire d'un document

- Export des données vers des tables en `.pdf`, `.xls` ou `.csv`
- Filtres et fonctions de recherche intuitifs et avancés

..
  - Génération automatique de documents de type PDF, MS-Word, OpenOffice et autres
    se basant sur des templates (modèles, gabarits)

  - WebDAV automatique intégré afin de pouvoir éditer des documents générés et
    stockés sur le serveur sans que l'utilisateur doive intervenir.
  - Authentication en utilisant votre annuaire LDAP
  - Système sophistiqué pour définir les workflows
