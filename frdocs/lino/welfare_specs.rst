==========================================
Charactéristiques du logiciel Lino Welfare
==========================================

.. ..contents::
  :depth: 1

Fonctionnalités générales
=========================

- Gestion des contacts des personnes et organisations
- Messagerie électronique
- Gestion des envois (courriers, mails, SMS, ...)

- Les utilisateurs peuvent donner procuration à leurs collègue de travailler et
  effectuer certaines actions en leur nom.

Gestion des bénéficiaires
=========================

- Les données signalétiques spécifiques aux CPAS (bénéficiaires, ménages,...)
  sont intégrées de manière transparente dans la gestion générale des contacts.

- Un bénéficiaire peut être accompagné par plusieurs intervenants dans des
  services différents.

- Lecture des données publiques de la carte d'identité électronique pour les
  importer dans la base de données (noms, date de naissance, addresse, validité,
  ... et photo).

Calendrier
==========

- Lino génère des propositions de rendez-vous pour les rencontres individuelles
  et les ateliers.

- Workflow pour gérer les invitations & rapports.
- Gestion des rendez-vous individuels ou en groupe.
- Jours fériés et congés individuels des agents.

Historique bénéficiaire
=======================

- Gestion des documents scannés (permis de conduire, de résidence, de
  travail, ...). Rappel optionnel si la date de fin de validité approche.

- Les travailleurs sociaux notent des observations. Traçabilité par exemple des
  appels téléphoniques, des rencontres internes et externes avec un
  bénéficiaire, collègues ou partenaires, courriers, ...

Réception
=========

- Les visiteurs s'enregistrent au guichet de réception et demandent une
  consultation avec un assistant social. L'agent d'accueil vérifie les données
  de contact du visiteur.

- Les visiteurs enregistrés restent dans la salle d'attente jusqu'à ce que
  l'assistant les reçoit. L'assistant enregistre également la fin de la
  consultation. Lino garde ces présences dans sa base de données.

- Les assistants sociaux peuvent consulter la file d'attente en temps réel
  depuis leur bureau.

- L'agent d'accueil peut consulter le calendrier des assistants sociaux et
  proposer des rendez-vous.

- L'agent d'accueil peut imprimer toute une série de documents tels que les
  attestations de présence ou les confirmations d'octroi d'aide.


Service d’insertion socio-professionnelle
=========================================

- Gestion des données relatives au CV (formations, expériences professionnelles,
  connaissances des langues, compétences, freins, ...)

- Outils de recherche sur base de ces critères pour trouver, face à une offre
  d'emploi, le candidat idéal sans devoir retourner dans les dossiers papiers
  pour passer en revue l'ensemble des bénéficiaires.

- Gestion des PIIS et projets de mise à l'emploi (articles 60§7 et 61): saisie,
  création automatique et impression du contrat papier, consultation, analyse
  statistique, rappels automatique des évaluations intermédiaires à prévoir.

- Gestion des partenaires art. 60§7, gestion des offres d'emploi vacants ainsi
- que des candidatures proposées par bénéficiaire ou par partenaire.

Nouvelles demandes
==================

- Gestion des compétences attribuées aux assistants sociaux.

- Proposition automatique des assistants sociaux compétents selon les
  spécificités de la demande.

- Attribution selon la disponibilité et la charge de travail des assistants.

Ateliers internes
=================

- Planning et calendrier des ateliers
- Inscription des bénéficiaires aux ateliers
- Gestion des présences

Cours de langues
================

- Gestion des organisations externes organisant des cours de langue
- Par bénéficiaire saisir des demandes de cours
- Par organisation une liste des candidats
- Gestion de l'offre et de la demande
- Liaison avec les contrats SISP

Médiation de dettes
===================

- Interface d'encodage des données (dépenses, revenus, obligations, répartition
  au marc-le-franc)

- Création automatique d'un budget individuel ou par ménage

Connexion BCSS
==============

- Données légales RN (IdentifyPerson)
- Intégration (ManageAccess)
- Registre National avec historique (Tx25)
- Rôle privilégié Conseiller en sécurité

Connexion SEPA
==============

- Lino importe les extraits de compte des bénéficiaires dont le compte est géré
  par le CPAS.

- Les assistants sociaux peuvent consulter ces données à tout moment.

Comptabilisation des dépenses sociales
======================================

- Encodage et envoi des ordres de paiement des aides sociales
- Encodage des entrées et sorties caisse relatives aux bénéficiaires
- Consultation de la situation comptable d'un bénéficiaire par rapport au CPAS
- Rapport quotidien et mensuel pour la comptabilité
