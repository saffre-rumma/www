=========
Bienvenue
=========

**Rumma & Ko OÜ** est une entreprise familiale située en Estonie et active surtout en Belgique.
Nous aidons nos clients à développer et maintenir leur propre application de base de données sur mesure
:doc:`en utilsant le framework Lino </services/lino4one>`.
Nous offrons également une sélection de
:doc:`solutions Lino pour les petits </services/lino4all>`.
Et ce :doc:`n'est pas tout </about/index>`.

.. toctree::
    :maxdepth: 1

    services/index
    lino/index
    sw/index
    about/index
